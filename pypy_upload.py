#!/usr/bin/env sh

rm -rf dist/
python3 setup.py sdist
pip3 install -U twine
twine upload dist/*
